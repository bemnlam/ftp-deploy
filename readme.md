## About node-ftp-connect
A tool try to automate deployment via FTP

### Problem: currently we don't have a CI/CD tool to deploy projects to UAT/LIVE
### Solution: build a raw script to automate this process while looking for a CI/CD solution

## Install
`npm install`

## Usage
0. setup *ftp-config.json*
  - you may create `ftp-config.json` file from `ftp-config.json.example`
  - set up uat config (you can add a *live* config for live deployment)
    * port: if port `22` is not working, try `21`
    * password: will prompt and request user to input when password is empty string
    * localRoot: should end with `/`

  - add the targeting site config
    * localRoot should start without `/`, and end with `/`
    * remoteRoot is the targeting FTP dir

1. run the script in powershell or bash etc.
  - `npm run uat` for uat config

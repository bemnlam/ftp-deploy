#!/usr/bin/env node --harmony
const co = require('co')
const prompt = require('co-prompt')
const program = require('commander')
const chalk = require('chalk')
// const ftpClient = require('ftp-client')
var ftpClient = require('ftp-deploy');
const ftpConfig = require('./ftp-config.json')
// const ftpOptions = require('./ftp-options.json')
const git = require('git-rev')
const gitUserName = require('git-user-name')
const inquirer = require('inquirer')
const fs = require('fs')
const log = console.log


// const siteCodes = [
//   'campaign-asia',
//   'corporate-treasurer'
// ]

program
.arguments('<env>')
.option('-u, --username <username>', 'ftp username')
.option('-p, --password <password>', 'ftp password')
.action(function(env) {
  if(!ftpConfig.hasOwnProperty(env)) {
    throw new Error(`[${env}] config does not exist.`);
  }
  var _config = ftpConfig[env]
  var _site = ftpConfig.site //ftpOptions

  const siteCodes = Object.keys(ftpConfig.site);

  // inquirer.prompt([{
  //   type: 'input',
  //   message: `username: `,
  //   name: 'user',
  //   default: _config.username
  // }]).then(answers => console.log(JSON.stringify(answers, null, '  ')))

  // if(!_config.password) {
  //   inquirer.prompt([{
  //     type: 'password',
  //     message: `password of ${_config.username}: `,
  //     name: 'pass'
  //   }]).then(answers => console.log(JSON.stringify(answers, null, '  ')))
  // }


    inquirer.prompt([
      {
        type: 'input',
        message: `username: `,
        name: 'username',
        default: _config.username
      },
      {
        type: 'password',
        message: `password: `,
        name: 'password',
        // default: _config.password
      },
      {
        type: 'rawlist',
        name: 'site',
        message: 'sitename: ',
        choices: siteCodes,
        // ['[choose a site]'].
        // validate: function(input) {
        //   //
        //   if(input === '[choose a site]')
        //   return true;
        // }
      }
    ]).then(function(answers){
      // log(JSON.stringify(answers, null, '  '));
      _site[answers.site].localRoot = _config.localRoot + _site[answers.site].localRoot
      var _deployConfig = Object.assign(_config, answers, _site[answers.site])
      _localRoot = _site[answers.site].localRoot;

      var ftpDeploy = new ftpClient();
      var question = chalk.yellow.bgBlack(`
site: ${env} ${answers.site}
from: ${_localRoot}
to:   ${_deployConfig.remoteRoot}
confirm? `)
      inquirer.prompt({
        type: 'confirm',
        name: 'confirm',
        message: question,
        default: true
      }).then(function(proceed) {
        if(!proceed.confirm) {
          log('user aborted')
          process.exit(0)
        }
        process.chdir(_localRoot)
        genVersionFile(_localRoot)

        ftpDeploy.deploy(_deployConfig, function(err) {
          if(err) log(err)
            else log('finished')
        })
      })

      ftpDeploy.on('uploading', function(data) {
        // data.totalFileCount;       // total file count being transferred
        // data.transferredFileCount; // number of files transferred
        // data.percentComplete;      // percent as a number 1 - 100
        // data.filename;             // partial path with filename being uploaded
        log('(%s/%s) %s', data.transferredFileCount + 1, data.totalFileCount, data.filename)
      });

      ftpDeploy.on('upload-error', function (data) {
        // data will also include filename, relativePath, and other goodies
        log('error when uploading %s (%s)', data.filename, data.err);
      });

    })

}).parse(process.argv);

function genVersionFile(path) {
  var filename = path + (path.endsWith('/') ? '' : '/') +'__version.txt'
  git.long(function(hash) {
    git.branch(function(branch) {
      var username = gitUserName()
      fs.writeFileSync(filename, `${hash}\r\n${branch}\r\n${username}`, { flag: 'w' }, function(err) {
        if(err) throw err
        log('version file generated')
      })
    })
  })
}
